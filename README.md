# NeoPixelMqtt #

## Backgrund ##
The original project where I control a Neopixel stripe with the Esp8266 by installing a webserver and selecting the color on a website. 

https://bitbucket.org/Remnis/neopixelwebserver

But this solution was not very flexible.
I installed a MQTT Broker on a Raspberry Pi and changed the code, so that it can exchange messages with the MQTT Broker.

## Video ##
https://youtu.be/SUYb0HOPXos